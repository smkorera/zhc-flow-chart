import { app, Menu , BrowserWindow, screen } from 'electron';
import * as path from 'path';
const fs = require('fs');
const os = require('os');
const electron = require('electron');
const windowManager = require('electron-window-manager');
const ipc = electron.ipcMain;
const shell = electron.shell;
 

let win, serve;
const args = process.argv.slice(1);
serve = args.some(val => val === '--serve');

if (serve) {
  require('electron-reload')(__dirname, {
  });
}




function createWindow() {

  const electronScreen = screen;
  const size = electronScreen.getPrimaryDisplay().workAreaSize;

  // Create browser window.
  win = new BrowserWindow({
    x: 0,
    y: 0,
    width: size.width,
    height: size.height
  });

  // and load the index.html of the app.
  win.loadURL('file://' + __dirname + '/index.html');

  // Open the DevTools.
  if (serve) {
    win.webContents.openDevTools();
  }

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store window
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });

  const menuTemp = Menu.buildFromTemplate([
      {
        label: 'Window',
        submenu: [
          {role: 'minimize'},
          {role: 'close'},
          {
            label: 'Print',
            click (item, focusedWindow) {
                const pdfPath = path.join(os.tmpdir(), 'print.pdf') ;             
                // Use default printing options
                win.webContents.printToPDF({ marginsType:2, pageSize:"A4", landscape:true }, function (error, data) {
                  if (error) throw error
                  fs.writeFile(pdfPath, data, function (error) {
                    if (error) {
                      throw error
                    }
                    shell.openExternal('file://' + pdfPath);
                   
                  })
                })

          }
          }
        ]
      }, // End File Menu Item

       {
        label: 'View',
        submenu: [
          {role: 'reload'},
          {role: 'forcereload'},
          {role: 'toggledevtools'},
          {type: 'separator'},
          {role: 'resetzoom'},
          {role: 'zoomin'},
          {role: 'zoomout'},
          {type: 'separator'},
          {role: 'togglefullscreen'}
        ]
      }

    ])

     Menu.setApplicationMenu(menuTemp);

}

try {

  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  app.on('ready', createWindow);

  // Quit when all windows are closed.
  app.on('window-all-closed', () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
      app.quit();
    }
  });

  app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
      createWindow();
    }
  });

} catch (e) {
  // Catch Error
  // throw e;
}
