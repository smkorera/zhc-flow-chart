export class Booking {
     BookingID: number;
     AgentID: number;
     BookingDate:  Date;
     WHCompany:  number;
     PaxName:  string ;
     Consultant: number;
     BookingRef:  number;
     ProvisionalBooking: number;
     GeneralNotes: string;
     InvoiceDate: Date;
     LastUpdated: number;
     DoneBy: number;
     Cancelled: number;
     Accounts: number;
     AccountsDate: Date;
     PreparedForAccountsBy: number;
     DocType: number;
     InvoiceNo: number;
     Currency: number;
     RateOfExchange: number;
     VAT: number;
     CreditNoteMemo: string;
     BkgFmRestoAccs: string;
     ThreeTenBooking: string;
     bookingoffice: number;
     parentbookingid: number;
     LastUpdatedBy: number;
     IsCashSale: number;
     Nationality: number;
     GroupName: string;
     CnoteRef: string;
     AutoEmailDate1:  Date;
     AutoEmailDate2:  Date ;
     AutoEmail1Sent: number;
     AutoEmail2Sent: number;
     RoomNo: number;
     CheckInDate: Date;
     CheckOutDate: Date;
     Cashed: number;
     CashupDate: Date;
     CreditNoteType: number;
     ProvisionalBookingPayDate: Date;
     PaymentDetail: string;
     REsstatus: number;
     Address1: string;
     Address2: string;
     City: string;
     PostalCode: string;
     Country: number;
     Tel: number;
     Fax: number;
     Ext1: number;
     Ext2: number;
     IDNo: number;
     Passport: number;
     CarReg: number;
     ParkingBay: number;
     Language: string;
     Email: string;
     Cell: number;
     DOB: Date;
     VATNo: number;
     LodgeBooking: number;
     ReconBookingID: number;
     POPLoaded: number;
     PaymentReceived: number;
     Salutation: string;
     SiteInspection: number;
     ResRequestID: number;
     Notes: string;
     PassPortNo: number;
     product_detail:Array<any>;
    
}