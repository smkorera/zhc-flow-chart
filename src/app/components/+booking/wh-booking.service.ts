import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map'

// import { AuthenticationService } from '../_services/index';
import { Booking } from './Booking';
import { AppSettings } from './../../app.config';

@Injectable()
export class BookingService {

   pagingObj: Object = {};
    constructor(
        private http: Http
               ) {}
    getBookings() {
        // add authorization header with jwt token
       // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        // let options = new RequestOptions({ headers: headers });

        // get bookings from api
        return this.http.get(AppSettings.API_ENDPOINT+'bookings')
            .map((response: Response) => response.json());
    }


      getBooking(BookingID: number) {
        // add authorization header with jwt token
       // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        // let options = new RequestOptions({ headers: headers });

        // get bookings from api
        return this.http.get(AppSettings.API_ENDPOINT+'booking/'+BookingID)
            .map((response: Response) => response.json());
    }


    filterBookings(PaxName: string) {
        // add authorization header with jwt token
       // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        // let options = new RequestOptions({ headers: headers });

        // get bookings from api
        return this.http.get(AppSettings.API_ENDPOINT+'booking/search/'+PaxName)
            .map((response: Response) => response.json());
    }

    paginateBooking (param: String, currentBookingID: Number) {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
        console.log(param);
         if(param === 'first') {
           this.pagingObj = {
            first: 'TEST',
            BookingID: currentBookingID
           }
        }
        else if(param === 'prior') {
           this.pagingObj = {
            prior: 'TEST',
            BookingID: currentBookingID
           }
        }else if(param === 'next') {
            this.pagingObj = {
            next: 'TEST',
            BookingID: currentBookingID
           }
        }else if(param === 'last') {
            this.pagingObj = {
            last: 'TEST',
            BookingID: currentBookingID
           }
        }


        return this.http.post(AppSettings.API_ENDPOINT+'booking/paginate', JSON.stringify(this.pagingObj), {headers})
            .map((response: Response) => response.json());

}

}
