import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map'

// import { AuthenticationService } from '../_services/index';
import { Consultant } from './Consultant';
import { AppSettings } from './../../app.config';

@Injectable()
export class ConsultantService {

   pagingObj :Object = {};
    constructor(
        private http: Http
               ) {}

    getConsultants() {
        // add authorization header with jwt token
       // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        // let options = new RequestOptions({ headers: headers });

        // get bookings from api
        return this.http.get(AppSettings.API_ENDPOINT+'consultants')
            .map((response: Response) => response.json());
    }

    paginateConsultant (param: String,currentConsultantID: Number) {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
        console.log(param);
         if(param == 'first'){
           this.pagingObj = {
            first: 'TEST',
            ConsultantID: currentConsultantID
           }
        }
        else if(param == 'prior'){
           this.pagingObj = {
            prior: 'TEST',
            ConsultantID: currentConsultantID
           }
        }else if(param == 'next'){
            this.pagingObj = {
            next: 'TEST',
            ConsultantID: currentConsultantID
           }
        }else if(param == 'last'){
            this.pagingObj = {
            last: 'TEST',
            ConsultantID: currentConsultantID
           }
        }


        return this.http.post(AppSettings.API_ENDPOINT+'consultant/paginate',JSON.stringify(this.pagingObj),{headers})
            .map((response: Response) => response.json());

}

}
