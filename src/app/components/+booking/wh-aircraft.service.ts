import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map'

// import { AuthenticationService } from '../_services/index';
import { Nationality } from './Nationality';
import { AppSettings } from './../../app.config';

@Injectable()
export class AircraftService {

   pagingObj :Object = {};
    constructor(
        private http: Http
               ) {}
    

    getAircrafts() {
        // add authorization header with jwt token
       // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        // let options = new RequestOptions({ headers: headers });
 
        // get bookings from api
        return this.http.get(AppSettings.API_ENDPOINT+'aircrafts')
            .map((response: Response) => response.json());
    }

    paginateAircraft (param: String,currentAircraftID: Number) {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
        console.log(param); 
         if(param == 'first'){
           this.pagingObj = {
            first: 'TEST',           
            AircraftID: currentAircraftID  
           }          
        }
        else if(param == 'prior'){
           this.pagingObj = {
            prior: 'TEST',           
             AircraftID: currentAircraftID 
           }  
        }else if(param == 'next'){
            this.pagingObj = {
            next: 'TEST',           
             AircraftID: currentAircraftID   
           }  
        }else if(param == 'last'){
            this.pagingObj = {
            last: 'TEST',          
              AircraftID: currentAircraftID   
           }  
        }               
    
      
        return this.http.post(AppSettings.API_ENDPOINT+'aircraft/paginate',JSON.stringify(this.pagingObj),{headers})
            .map((response: Response) => response.json());
    
}

}
