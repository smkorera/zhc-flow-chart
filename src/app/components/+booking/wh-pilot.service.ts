import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map'

// import { AuthenticationService } from '../_services/index';
import { Nationality } from './Nationality';
import { AppSettings } from './../../app.config';

@Injectable()
export class PilotService {

   pagingObj :Object = {};
    constructor(
        private http: Http
               ) {}
    

    getPilots() {
        // add authorization header with jwt token
       // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        // let options = new RequestOptions({ headers: headers });
 
        // get bookings from api
        return this.http.get(AppSettings.API_ENDPOINT+'pilots')
            .map((response: Response) => response.json());
    }

    paginatePilot (param: String,currentPilotID: Number) {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
        console.log(param); 
         if(param == 'first'){
           this.pagingObj = {
            first: 'TEST',           
            PilotID: currentPilotID  
           }          
        }
        else if(param == 'prior'){
           this.pagingObj = {
            prior: 'TEST',           
           PilotID: currentPilotID  
           }  
        }else if(param == 'next'){
            this.pagingObj = {
            next: 'TEST',           
            PilotID: currentPilotID  
           }  
        }else if(param == 'last'){
            this.pagingObj = {
            last: 'TEST',          
             PilotID: currentPilotID  
           }  
        }               
    
      
        return this.http.post(AppSettings.API_ENDPOINT+'pilot/paginate',JSON.stringify(this.pagingObj),{headers})
            .map((response: Response) => response.json());
    
}

}
