import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map'

// import { AuthenticationService } from '../_services/index';
import { Currency } from './Currency';
import { AppSettings } from './../../app.config';

@Injectable()
export class CurrencyService {

   pagingObj: Object = {};
    constructor(
        private http: Http
               ) {}


    getCurrencies() {
        // add authorization header with jwt token
       // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        // let options = new RequestOptions({ headers: headers });

        // get bookings from api
        return this.http.get(AppSettings.API_ENDPOINT+'currencies')
            .map((response: Response) => response.json());
    }

    paginateCurrency (param: String,currentCurrencyID: Number) {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
        console.log(param);
         if(param == 'first'){
           this.pagingObj = {
            first: 'TEST',
            CurrencyID: currentCurrencyID
           }
        }
        else if(param == 'prior'){
           this.pagingObj = {
            prior: 'TEST',
            CurrencyID: currentCurrencyID
           }
        }else if(param == 'next'){
            this.pagingObj = {
            next: 'TEST',
            CurrencyID: currentCurrencyID
           }
        }else if(param == 'last'){
            this.pagingObj = {
            last: 'TEST',
            CurrencyID: currentCurrencyID
           }
        }


        return this.http.post(AppSettings.API_ENDPOINT+'currency/paginate',JSON.stringify(this.pagingObj),{headers})
            .map((response: Response) => response.json());

}

}
