import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map'

// import { AuthenticationService } from '../_services/index';
import { Location } from './Location';
import { AppSettings } from './../../app.config';

@Injectable()
export class LocationService {

   pagingObj :Object = {};
    constructor(
        private http: Http
               ) {}


    getLocations() {
        // add authorization header with jwt token
       // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        // let options = new RequestOptions({ headers: headers });

        // get bookings from api
        return this.http.get(AppSettings.API_ENDPOINT+'transfers')
            .map((response: Response) => response.json());
    }

    paginateLocation (param: String,currentLocationID: Number) {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
        console.log(param);
         if(param == 'first'){
           this.pagingObj = {
            first: 'TEST',
            LocationID: currentLocationID
           }
        }
        else if(param == 'prior'){
           this.pagingObj = {
            prior: 'TEST',
            LocationID: currentLocationID
           }
        }else if(param == 'next'){
            this.pagingObj = {
            next: 'TEST',
            LocationID: currentLocationID
           }
        }else if(param == 'last'){
            this.pagingObj = {
            last: 'TEST',
            LocationID: currentLocationID
           }
        }


        return this.http.post(AppSettings.API_ENDPOINT+'transfer/paginate',JSON.stringify(this.pagingObj),{headers})
            .map((response: Response) => response.json());

}

}
