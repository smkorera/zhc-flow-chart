import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map'
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  isToday,
  isAfter,
  isBefore,
  isWithinRange,
  addHours,
  format
} from 'date-fns';

// import { AuthenticationService } from '../_services/index';
import { AppSettings } from './../../app.config';


@Injectable()
export class FlightAssignService {

	 pagingObj: Object = {};
	 Obj :Object       = {};
	 flightObj :Object       = {};

		constructor(
				private http: Http
							 ) {}
		getFlightAssigns() {
				// add authorization header with jwt token
			 // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
				// let options = new RequestOptions({ headers: headers });

				// get bookings from api'
				return this.http.get(AppSettings.API_ENDPOINT+'flightassigns')
						.map((response: Response) => response.json());
		}

		getPaxRecon(reportDate:any){

			// get bookings from api'
				return this.http.get(AppSettings.API_ENDPOINT+'flightassign/paxrecon/'+reportDate)
						.map((response: Response) => response.json());

		}



	 closeDay(data:any,reportDate:any){     
         const headers = new Headers();
         headers.append('Content-Type', 'application/json');   
         

           console.log(data);
        
        return this.http.put(AppSettings.API_ENDPOINT+'flightassign/closeday/'+reportDate,JSON.stringify(data), {headers})
            .map((response: Response) => response.json());
       }


       updatePax(data:any ){

       	const headers = new Headers();
       	headers.append('Content-Type','application/json');
       	  return this.http.put(AppSettings.API_ENDPOINT+'flightassign/closeday/'+data.BookingLineID,JSON.stringify(data), {headers})
            .map((response: Response) => response.json());

       }


		saveFlightAssigns(data:any) {
				// add authorization header with jwt token
			 // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
				// let options = new RequestOptions({ headers: headers });

				// get bookings from api

			 const headers = new Headers();
			 headers.append('Content-Type', 'application/json');

				this.flightObj = {
						flightAssignID:data.flightAssignID,
						Pilot: data.pilot,
						Aircraft: data.aircraft,
						Time: this.toHHMMSS(data.start),
						Date: format(data.start,'YYYY-MM-DD'),
						pax : 4,
						newPax:0,					
						bookingid:12				
					
					 }

					 console.log(this.flightObj);
				return this.http.post(AppSettings.API_ENDPOINT+'tempflightassign',JSON.stringify(this.flightObj),{ headers})
						.map((response: Response) => response.json());
		}


		getFlightAssign(BookingID: String) {
				// add authorization header with jwt token
			 // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
				// let options = new RequestOptions({ headers: headers });

				// get bookings from api
				return this.http.get(AppSettings.API_ENDPOINT+'flightassign/'+BookingID)
						.map((response: Response) => response.json());
		}

		updateFlightAssign(data :any,pax:any ,paxEdited:any) {
			 console.log("in service");
			  console.log(paxEdited);
			 //console.log(data);
			 const headers = new Headers();
			 headers.append('Content-Type', 'application/json');

					 if(data.event.final == 0 || data.event.final ===0 ||data.event.final == undefined){
					 	this.flightObj = {
						flightAssignID:data.event.flightAssignID,
						Pilot: data.event.pilot.staffid,
						Aircraft: data.event.aircraft.ID,
						Time: this.toHHMMSS(data.event.start),
						Date: format(data.event.start,'YYYY-MM-DD'),
						pax :pax? (data.event.meta.pax-pax) :paxEdited,
						newPax:pax,
						voucher:data.event.voucher,
						bookingid:data.event.bookingid,
						FlightNumber:data.event.FlightNumber
						
					
					 }
					 }else {
					 	this.flightObj = {
						flightAssignID:data.event.flightAssignID,
						Pilot: data.event.pilot.staffid,
						Aircraft: data.event.aircraft.ID,
						Time: this.toHHMMSS(data.event.start),
						Date: format(data.event.start,'YYYY-MM-DD'),
						pax :pax? (data.event.meta.pax-pax) :paxEdited,
						newPax:pax,
						voucher:data.event.voucher,
						bookingid:data.event.bookingid,
						Final:data.event.final,
						FlightNumber:data.event.FlightNumber
					 }
					 }

					 console.log(this.flightObj);

				// get bookings from api
				return this.http.post(AppSettings.API_ENDPOINT+'flightassign',JSON.stringify(this.flightObj), {headers})
						.map((response: Response) => response.json());
		}

		getFlightAssignsByAircraft(AircraftID: number) {

			const headers = new Headers();
			headers.append('Content-Type', 'application/json');

					 this.Obj = {
						AircraftID: AircraftID
					 }


			return this.http.post(AppSettings.API_ENDPOINT+'flightassigns',JSON.stringify(this.Obj), {headers})
			.map((response: Response) => response.json());

		}

		paginateFlightAssign (param: String, currentBookingID: Number) {
			const headers = new Headers();
			headers.append('Content-Type', 'application/json');
				console.log(param);
				 if(param === 'first') {
					 this.pagingObj = {
						first: 'TEST',
						BookingID: currentBookingID
					 }
				}
				else if(param === 'prior') {
					 this.pagingObj = {
						prior: 'TEST',
						BookingID: currentBookingID
					 }
				}else if(param === 'next') {
						this.pagingObj = {
						next: 'TEST',
						BookingID: currentBookingID
					 }
				}else if(param === 'last') {
						this.pagingObj = {
						last: 'TEST',
						BookingID: currentBookingID
					 }
				}


				return this.http.post(AppSettings.API_ENDPOINT+'flightassign/paginate', JSON.stringify(this.pagingObj), {headers})
						.map((response: Response) => response.json());

}

 toHHMMSS = (date) => {

	var HH = this.addZero(date.getHours());
	var MM = this.addZero(date.getMinutes());
	var SS = this.addZero(date.getSeconds());
				return HH+":"+MM +":"+SS;
}

 addZero(i) {
		if (i < 10) {
				i = "0" + i;
		}
		return i;
}

}
