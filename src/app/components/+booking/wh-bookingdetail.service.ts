import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map'

// import { AuthenticationService } from '../_services/index';
import { BookingDetail } from './BookingDetail';
import { AppSettings } from './../../app.config';

@Injectable()
export class BookingDetailService {

   pagingObj: Object = {};
   Obj :Object       = {};

    constructor(
        private http: Http
               ) {}
    getBookingDetails() {
        // add authorization header with jwt token
       // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        // let options = new RequestOptions({ headers: headers });

        // get bookings from api
        return this.http.get(AppSettings.API_ENDPOINT+'bookingdetails')
            .map((response: Response) => response.json());
    }

    getBookingDetail(BookingID: String) {
        // add authorization header with jwt token
       // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        // let options = new RequestOptions({ headers: headers });

        // get bookings from api
        return this.http.get(AppSettings.API_ENDPOINT+'bookingdetail/'+BookingID)
            .map((response: Response) => response.json());
    }

    getBookingDetailsByAircraft(AircraftID: number) {

      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
        
           this.Obj = {          
            AircraftID: AircraftID
           }
        

      return this.http.post(AppSettings.API_ENDPOINT+'bookingdetails',JSON.stringify(this.Obj), {headers})
      .map((response: Response) => response.json());

    }

    paginateBookingDetail (param: String, currentBookingID: Number) {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
        console.log(param);
         if(param === 'first') {
           this.pagingObj = {
            first: 'TEST',
            BookingID: currentBookingID
           }
        }
        else if(param === 'prior') {
           this.pagingObj = {
            prior: 'TEST',
            BookingID: currentBookingID
           }
        }else if(param === 'next') {
            this.pagingObj = {
            next: 'TEST',
            BookingID: currentBookingID
           }
        }else if(param === 'last') {
            this.pagingObj = {
            last: 'TEST',
            BookingID: currentBookingID
           }
        }


        return this.http.post(AppSettings.API_ENDPOINT+'bookingdetail/paginate', JSON.stringify(this.pagingObj), {headers})
            .map((response: Response) => response.json());

}

}
