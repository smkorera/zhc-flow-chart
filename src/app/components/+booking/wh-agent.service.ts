import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map'

// import { AuthenticationService } from '../_services/index';
import { Agent } from './Agent';
import { AppSettings } from './../../app.config';

@Injectable()
export class AgentService {

   pagingObj :Object = {};
    constructor(
        private http: Http
               ) {}
    

    getAgents() {
        // add authorization header with jwt token
       // let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        // let options = new RequestOptions({ headers: headers });
 
        // get bookings from api
        return this.http.get(AppSettings.API_ENDPOINT+'agents')
            .map((response: Response) => response.json());
    }

    paginateAgent (param: String,currentAgentID: Number) {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
        console.log(param); 
         if(param == 'first'){
           this.pagingObj = {
            first: 'TEST',           
            AgentID: currentAgentID  
           }          
        }
        else if(param == 'prior'){
           this.pagingObj = {
            prior: 'TEST',           
            AgentID: currentAgentID  
           }  
        }else if(param == 'next'){
            this.pagingObj = {
            next: 'TEST',           
            AgentID: currentAgentID  
           }  
        }else if(param == 'last'){
            this.pagingObj = {
            last: 'TEST',          
            AgentID: currentAgentID  
           }  
        }               
    
      
        return this.http.post(AppSettings.API_ENDPOINT+'agent/paginate',JSON.stringify(this.pagingObj),{headers})
            .map((response: Response) => response.json());
    
}

}
