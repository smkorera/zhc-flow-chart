import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';


@Injectable()
export class LoginService {

  constructor(private http:HttpClient) { }

  CheckLogin(item :any){   
    console.log(item) 
           const headers = new HttpHeaders();
          headers.append('Content-Type', 'application/json');
    
             return this.http.post('http://192.168.10.1/flow%20chart/public/api/auth/login', item,{ headers})
                .map((response: Response) => response);
        }

  LogOut(){
    localStorage.removeItem('currentUser');
  }


}
