import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './wh-login.component.html',
  styleUrls: ['./wh-login.component.css']
})
export class LoginComponent {

  @Input() public name: string;
  @Input() public age: number;
  @Input() public department: string;
  @Input() public username: string;
  @Input() public password:string;

  listItems: Array<string> = ['Cruises','Rafting', 'Canoeing', 'High Wire', 'Elephants', 'The Elephant Camp','Reservations'];
}
