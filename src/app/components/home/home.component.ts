import {
  Component,
  OnInit,
  Input, NgZone , ChangeDetectorRef,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef } from '@angular/core';

import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  isSameHour,
  isSameMinute,
  isToday,
  isAfter,
  isBefore,
  isWithinRange,
  addHours,
  format
} from 'date-fns';

import { Subject } from 'rxjs/Subject';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import {
  PopupService,
  PopupRef
} from '@progress/kendo-angular-popup';

import { LoginComponent } from '../+login/wh-login.component';
import { LoginService } from '../+login/login.service';




import {
  DialogService,
  DialogRef,
  DialogCloseResult
} from '@progress/kendo-angular-dialog';

import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarEventTitleFormatter,
  CalendarDateFormatter
} from 'angular-calendar';
import { CustomEventTitleFormatter } from './custom-event-title-formatter.provider';
import { CustomDateFormatter } from './custom-date-formatter.provider';


import { BookingDetailService } from '../+booking/wh-bookingdetail.service';
import { FlightAssignService } from '../+booking/wh-flightassign.service';
import { PilotService } from '../+booking/wh-pilot.service';
import { AircraftService } from '../+booking/wh-aircraft.service';
import { BookingDetail } from '../+booking/BookingDetail';
import { MyEvent } from './MyEvent';


@Component({
  selector: 'app-home',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomEventTitleFormatter
    },
    {
    provide: CalendarDateFormatter,
    useClass: CustomDateFormatter
  }
  ]
})
export class HomeComponent implements OnInit {

  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  view: string = 'day';
  currentAircraft :any;
  currentAircraftID :number = 7;
  newPax:number ;
  paxEdit:number;
  voucher:any;
  newEvent: any;
  errorPax:any = false;
  total:number =  0;
  public final: boolean = true;
  checkFinal:any ;
  errorMessage:any;
  successMessage:any;
  fTotal  = 0;
  dbTotal = 0;
  error :any;
  message:any;

   private popupRef: PopupRef;

  viewDate: Date = new Date();

  data: Array<any> = [];
  paxReconItems:Array<any> = [];

  colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
    pilot:null
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
    pilot:null
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
    pilot:null
  }
};

  modalData: {
    action: string;
    event: MyEvent;
  };


  dragData: {   
    event: any;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: MyEvent }): void => {
         this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: MyEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
         this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  @Input() events: MyEvent[] = [];
   @Input() eventsTemp: MyEvent[] = [];
   @Input() pilotItems: Array<any>            = [];
    @Input() aircraftItems: Array<any>          = [];


  activeDayIsOpen: boolean = true;

  constructor(
    private modal: NgbModal,
    private bookingDetailService: BookingDetailService,
    private pilotService: PilotService,
    private aircraftService: AircraftService,
    private popupService: PopupService,
    private flightAssignService: FlightAssignService,
    private dialogService: DialogService,
    private loginService: LoginService,
    public zone: NgZone,
    private ref: ChangeDetectorRef) { }

   dayClicked({ date, events }: { date: Date; events: MyEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }

    console.log(this.viewDate);

    this.paxReconItems = [];
      let reportDate = format(this.viewDate,"YYYY-MM-DD");
      this.flightAssignService.getPaxRecon(reportDate)
          .subscribe(result=>{
            this.paxReconItems = result.paxrecon;
             this.refresh.next();
          })
  }

 

  

  handleEvent(action: string, event: MyEvent): void {
    this.checkFinal = event.final;
    this.modalData = { event, action };
    console.log("setting event pax");
    this.paxEdit   = this.modalData.event.meta.pax;
    this.voucher   = this.modalData.event.voucher;
    console.log(this.paxEdit);
    this.modal.open(this.modalContent, { size: 'lg' });
  }


  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    // 

    if(isSameHour(newStart,event.start) && isSameMinute(newStart,event.start)){

    }else {

    event.start = newStart;
    event.end = newEnd;
    // update time on server
    this.dragData = { event };
    console.log("saving since times are different");
      
     
     this.paxEdit   = this.dragData.event.meta.pax;
     this.flightAssignService.updateFlightAssign(this.dragData,this.newPax ,this.paxEdit)
    .subscribe(result => {
        console.log("finished saving")
        this.dragData = undefined;
        this.paxEdit = undefined;
        this.newPax = undefined;
        //this.refresh.next();
    });

    this.refresh.next();

    }
    
  }





  addEvent(date: Date): void {

    const dialog: DialogRef = this.dialogService.open({
          title: this.message,

          // Show component
          content: PilotComponent,

          actions: [
              { text: "Cancel" },
              { text: "Proceed", primary: true }
          ],
          width: 450,
          height: 200
          
      }); 



   dialog.result.subscribe((result) => {

         console.log(result);
          if (result instanceof DialogCloseResult) {
              console.log("close");
              this.ngOnInit();
          } else {
             
              const pilotInfo = dialog.content.instance;
              let pilotid = pilotInfo.pilot.staffid;            

               console.log("action", pilotInfo);

              console.log(date);
              let startDate = new Date(date);
               let endDate   = new Date(date);
                   endDate.setMinutes(startDate.getMinutes()+15);

                var event = {
                title: 'NULL FLIGHT',
                start: startDate,
                end: endDate,
                color: this.colors.red,
                draggable: true,
                pilot:pilotid,
                aircraft:this.currentAircraft,
                resizable: {
                  beforeStart: true,
                  afterEnd: true
                },
                flightAssignID:0,
                voucher:0,
                bookingid:0,
                final:1,
                FlightNumber:" ",
                IsTempFlight:1
              };
               console.log(event);
               //this.events.push(event);
              ////this.refresh.next();

              //save to db 

              this.flightAssignService.saveFlightAssigns(event)
              .subscribe(result => {
                 this.events = [];
                  this.eventsTemp = [];   
                 console.log(result) ;  
                 this.newPax = undefined ;this.paxEdit =  undefined;


              });

              //this.refresh.next();
              
               this.flightAssignService.getFlightAssignsByAircraft(this.currentAircraft)
              .subscribe(result => {
                this.events = [];
                this.eventsTemp = [];
                  //console.log(result);
                  this.data = result.flightAssigns;
                  this.formartData();

              });
             
          }

          //this.result = JSON.stringify(result);
      });


    

  }

  ngOnInit() {


    const dialog: DialogRef = this.dialogService.open({
          title: this.message,

          // Show component
          content: LoginComponent,

          actions: [
              { text: "Cancel" },
              { text: "Proceed", primary: true }
          ],
          width: 450,
          height: 350
          
      }); 



   dialog.result.subscribe((result) => {

         console.log(result);
          if (result instanceof DialogCloseResult) {
              console.log("close");
              this.ngOnInit();
          } else {
             
              const userInfo = dialog.content.instance;
              let username = userInfo.username;
              let password = userInfo.password;

               console.log("action", userInfo);

                  if (localStorage.getItem('currentUser')) {
                    //this.router.navigate(['/']);
                  } else {
                    this.loginService.CheckLogin({username:username,password:password}).subscribe(result => {
                      console.log(result);
                      if (result.status === 304 ) {
                        this.error = result.statusText;
                        this.message = result.statusText;
                        this.ngOnInit();


                      } else if (result.status === 200) {
                          this.setCacheValue(result);
                          //this.router.navigate(['/']);

                      } else if (result.status === 404) {
                        this.error = result.statusText;
                        this.message = result.statusText;
                      } else {
                        this.error = 'Oops something went wrong';
                        this.message = 'Oops something went wrong';
                      }
                    });
                  }
              // fire post request to login api
              // handle logins
              // if wrong password or username reopen login component
              // else proceed and store username in localstorage
             
          }

          //this.result = JSON.stringify(result);
      });

   this.selectAircraft(0);
   this.pilotService.getPilots()
        .subscribe(result => {
            this.pilotItems = result.pilots;
             console.log(result);
        });

      this.aircraftService.getAircrafts()
          .subscribe(result => {
              this.aircraftItems = result.aircrafts;
               console.log(result);
      });

     

  }

  formartData() {
    this.data.map(item => {

      let startDate = new Date(item.Date+' '+item.Time);
      let endDate   = new Date(item.Date+' '+item.Time);

        //set Flight start Time to 7.00;
      let startFlightTime = new Date(item.Date);
          startFlightTime.setHours(7,0,0);
        
          //set Flight End TO 1700 HRS

       let endFlightTime = new Date(item.Date);
           endFlightTime.setHours(17,0,0);       

           // if date is not within range 7:00 - 17:00 set time to 7:00  \

       let tempDate = new Date(item.Date);
       
       let tempTime = new Date(item.Date)
       let  timeArray     = item.Time.split(":") ;
       let hh = timeArray[0];
       let mm = timeArray[1];
       let ss = timeArray[2];

       tempDate.setHours(hh);
       tempTime.setMinutes(mm);

      let isWithin = isWithinRange(tempDate,startFlightTime,endFlightTime);
       if(isWithin){
          console.log(true);
       } else{
         tempDate.setHours(7,0,0);     
         startDate  = tempDate;
         startDate.setHours(7,0,0);
         endDate = startDate;
          console.log(item.Paxname);
         console.log(startDate);

       }    
      let currentDate = new Date(item.Date+' '+item.Time);          

      
        // if(currentDate.getHours()>startFlightTime.getHours() && currentDate.getHours()<endFlightTime.getHours()){
          

        // }else{
        //    startDate = new Date();
        //    endDate   = new Date();
        //    startDate.setHours(8,0,0); 
               
        //    //   let endDate   =  new Date(item.Date+' '+item.Time);
        // }
      



      // if(currentDate <endFlightTime && currentDate > startFlightTime){
      //   console.log("not dissapear");
      // }else{
      //   console.log("dissapeared");
      // }

           // if(isWithinRange(item.Date,startFlightTime,endFlightTime)){
           //   let startDate =  new Date(item.Date+' '+item.Time);         
           //   let endDate   =  new Date(item.Date+' '+item.Time);

           // }else{
           //  // console.log(item);
           //   let startDate =  new Date(); 
           //       startDate.setHours(7,0,0);        
           //   let endDate   =  new Date(item.Date+' '+item.Time);
           // }

       if(!item.Time){
            item.Time ="07:00:00";
       } 

       if(item.product_detail == null){
         console.log(item);
       }       

      if(item.product_detail.product_or_service.duration <15) {   
         endDate.setMinutes(startDate.getMinutes()+15);
         var cc = this.colors.red;
      }else if(item.product_detail.product_or_service.duration >15 && item.product_detail.product_or_service.duration < 30) {
         endDate.setMinutes(startDate.getMinutes()+30);
         var cc = this.colors.yellow;
      }else {
         endDate.setMinutes(startDate.getMinutes()+60);
         var cc = this.colors.blue;
      }

      if(item.product_detail.booking.Notes){
        var tt = item.Paxname+'  '+item.product_detail.transfer_from.description+'@'+item.product_detail.PickTime+'  Pax '+item.Pax+' ('+item.product_detail.booking.agent.Agent+'  vch#'+item.product_detail.booking.BookingRef+' '+item.product_detail.booking.agent_consultant.Name+' '+item.product_detail.booking.Notes+')'
      }
      else {
        var tt = item.Paxname+'  '+item.product_detail.transfer_from.description+'@'+item.product_detail.PickTime+'  Pax '+item.Pax+' ('+item.product_detail.booking.agent.Agent+'  vch#'+item.product_detail.booking.BookingRef+' '+item.product_detail.booking.agent_consultant.Name+')'
      }

      return {
        start: startDate,
        end: endDate,
        title: tt,
        color: cc,
        actions: this.actions,
        draggable:item.pilot.name ,
        pilot:item.pilot,
        aircraft:item.aircraft,
        flightAssignID:item.ID,
        voucher:item.product_detail.booking.BookingRef,
        bookingid:item.product_detail.booking.BookingID,
        final:item.Final,
        FlightNumber:item.FlightID,
        IsTempFlight:item.IsTempFlight,
        meta:{
         pax:item.Pax,
         
        }
      }

    

  }).forEach(item => {
   
    this.eventsTemp.push(item);
    if(isToday(item.start)){
    this.total += item.meta.pax;

  }


   
  });


  
  //this.events = this.eventsTemp;
  this.zone.run(() => this.events = this.eventsTemp);
  ChangeDetectorRef.detectChanges();
  this.ref.markForCheck();

  this.refresh.next();


  }

  selectAircraft(value) {
    this.events = [];
this.eventsTemp = [];

    this.total = 0;
    let aircraft = 0;

   // get details by aircraft
   if (value.index === 0) {
     aircraft = 7;
     
   } else if(value.index === 1){
      aircraft = 8;
   }else if(value.index === 2){
      aircraft = 9;
   }else if(value.index === 3){
      aircraft = 11;
   }   else {
      aircraft = 7;
   }

    if(value.index === 4){
      this.paxReconItems = [];
      let reportDate = format(this.viewDate,"YYYY-MM-DD");
      this.flightAssignService.getPaxRecon(reportDate)
          .subscribe(result=>{
            this.paxReconItems = result.paxrecon;
            console.log(this.paxReconItems);
          })
   }

   this.currentAircraft = aircraft;
  // let aircraft = (value.index === 0) ? (7) : ((value.index === 1) ? (8) : ((value.index === 2) ? (9) : (value.index === 3) ? (11) : (7)))

   console.log(aircraft);

   this.flightAssignService.getFlightAssignsByAircraft(aircraft)
    .subscribe(result => {
         this.events = [];
this.eventsTemp = [];
        //console.log(result);
        this.data = result.flightAssigns;
        this.formartData();

    });

   


  }

  save(event){   

     this.events = [];
     this.eventsTemp = [];

     event.event.meta.pax = this.paxEdit;
     event.event.voucher  = this.voucher;
     //event.event.start = format(event.event.start,'YYYY-MM-DD');

      console.log(event);
    this.flightAssignService.updateFlightAssign(event,this.newPax,this.paxEdit)
    .subscribe(result => {
       this.events = [];
       this.eventsTemp = [];
       console.log(result);
       this.newPax = undefined ;this.paxEdit =  undefined;


    });

    

   
     this.flightAssignService.getFlightAssignsByAircraft(this.currentAircraft)
    .subscribe(result => {
        //this.events = [];
this.eventsTemp = [];
        //console.log(result);
        this.data = result.flightAssigns;
        this.formartData();


    });

  }


  saveCopy(event ,template: TemplateRef<any>){

    console.log(this.modalData);
    this.newEvent = this.modalData;

     if (this.popupRef) {
            this.popupRef.close();
            this.popupRef = null;
        } else {
            this.popupRef = this.popupService.open({
                content: template,
                offset: { top: 83, left: 400 }
            });
        }

    

  }


   togglePopup(template: TemplateRef<any>) {
        if (this.popupRef) {
            this.popupRef.close();
            this.popupRef = null;
        } else {
            this.popupRef = this.popupService.open({
                content: template,
                offset: { top: 83, left: 400 }
            });
        }
    }


    update(){

      this.events = [];
this.eventsTemp = [];

      if(this.newPax >= this.modalData.event.meta.pax){
         
         this.errorPax = true; 
         return 0;
      }

    this.flightAssignService.updateFlightAssign(this.modalData,this.newPax ,this.paxEdit)
    .subscribe(result => {
        console.log(result);
        console.log("finished saving lets close popup");
        this.newPax = undefined; this.paxEdit =undefined;
    });

    

   
    this.events = [];
this.eventsTemp = [];
     this.flightAssignService.getFlightAssignsByAircraft(this.currentAircraft)
    .subscribe(result => {

        //console.log(result);
        this.data = result.flightAssigns;
        this.formartData();

    });

            this.popupRef.close();
            this.popupRef = null;
            this.newPax   = null;
            //this.refresh.next();
    }


   onKey(value: number) {
    this.paxEdit = value;
  }

  valueChange(value){
   console.log(value);
   this.voucher = value;
  }


  updatePax(value){
   console.log(value);
   this.flightAssignService.updatePax(value).subscribe(res=>{
    console.log(res);
   })
   //this.voucher = value;
  }

  closeDay(){
    // console.log(this.paxReconItems);
    this.fTotal = 0;
    this.dbTotal = 0;
    
    let reportDate = format(this.viewDate,"YYYY-MM-DD");
    var result = this.paxReconItems.map(a=> a.BookingLineID);
    var FlowChartTotal = this.paxReconItems.forEach(item => { 
     // console.log(item);     
      this.fTotal += parseInt(item.FlowChartPax);
      this.dbTotal +=parseInt(item.ActualPax);
     });     

     if(this.fTotal!=this.dbTotal){
      this.errorMessage = "Pax do not match";
      //console.log(this.fTotal);
      //console.log(this.dbTotal);
     }else {

     this.successMessage = "Day completed successfully Thank you";
      
      this.flightAssignService.closeDay(result,reportDate).subscribe(res=>{
        console.log(res);
        this.successMessage = "Completed Successfully Thank you";
        this.paxReconItems = [];
        this.refresh.next();

     })
     }

    

    
    
  }

    getFlowChartTotal(){
    // console.log(this.paxReconItems);
    this.fTotal = 0;
    this.dbTotal = 0;
    
    let reportDate = format(this.viewDate,"YYYY-MM-DD");
    var result = this.paxReconItems.map(a=> a.BookingLineID);
    var FlowChartTotal = this.paxReconItems.forEach(item => { 
     // console.log(item);     
      this.fTotal += parseInt(item.FlowChartPax);
     
     });     

     return this.fTotal;

    
    
  }


  getPOSTotal(){
    // console.log(this.paxReconItems);
    this.fTotal = 0;
    this.dbTotal = 0;
    
    let reportDate = format(this.viewDate,"YYYY-MM-DD");
    var result = this.paxReconItems.map(a=> a.BookingLineID);
    var FlowChartTotal = this.paxReconItems.forEach(item => { 
     // console.log(item);     
      this.fTotal += parseInt(item.ActualPax);
     
     });     

     return this.fTotal;

    
    
  }




  getTotal(item){
     
  }

   public setCacheValue(user: any): void {
    console.log(user.data);
    localStorage.setItem('currentUser', JSON.stringify(user.data));

  }

   
}


@Component({
  selector: 'app-pilot',
  templateUrl: './wh-pilot.component.html',
  styleUrls: ['./wh-pilot.component.css']
})
export class PilotComponent implements OnInit{

  @Input() public pilot: any;
           public listItems: Array<string> = [];

  constructor(private pilotService: PilotService) {}


 
 
  ngOnInit(){

    this.pilotService.getPilots()
        .subscribe(result => {
            this.listItems = result.pilots;
             console.log(result);
        });
  }
}

