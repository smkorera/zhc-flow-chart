import { CalendarEventTitleFormatter, CalendarEvent } from 'angular-calendar';

export interface MyEvent extends CalendarEvent {
  pilot: any ;
  aircraft: any ;
  flightAssignID:any;
  voucher:any;
  bookingid:any;
  final:any;
  FlightNumber:any;
  IsTempFlight:any
}
