import { LOCALE_ID, Inject } from '@angular/core';
import { CalendarEventTitleFormatter, CalendarEvent } from 'angular-calendar';
import { MyEvent } from './MyEvent';

export class CustomEventTitleFormatter extends CalendarEventTitleFormatter {
  constructor(@Inject(LOCALE_ID) private locale: string) {
    super();
  }

  // you can override any of the methods defined in the parent class

  month(event: MyEvent): string {
    return `<b>${new Intl.DateTimeFormat(this.locale, {
      hour: 'numeric',
      minute: 'numeric'
    }).format(event.start)}</b> ${event.title}`;
  }

  week(event: MyEvent): string {
    return `<b>${new Intl.DateTimeFormat(this.locale, {
      hour: 'numeric',
      minute: 'numeric'
    }).format(event.start)}</b> ${event.title}`;
  }

  day(event: MyEvent): string {
    //console.log(event);
    if(event.FlightNumber){
      return `<b>${event.FlightNumber} ${new Intl.DateTimeFormat(this.locale, {
      hour: 'numeric',
      minute: 'numeric'
    }).format(event.start)} ${event.draggable}  </b><br> ${event.title}`;
  }else {
    return `<b> ${new Intl.DateTimeFormat(this.locale, {
      hour: 'numeric',
      minute: 'numeric'
    }).format(event.start)} ${event.draggable}  </b><br> ${event.title}`;
  }

    
  }
}
