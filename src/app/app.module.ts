import 'zone.js/dist/zone-mix';
import 'reflect-metadata';
import 'polyfills';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';

import { CalendarModule } from 'angular-calendar';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { DemoUtilsModule } from './demo-utils/module';


import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Jsonp, JsonpModule } from '@angular/http';
import { HttpClientModule ,HttpClient } from '@angular/common/http'; 


import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { ChartsModule } from '@progress/kendo-angular-charts';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { GridModule } from '@progress/kendo-angular-grid';
import { LabelModule } from '@progress/kendo-angular-label';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { PopupModule } from '@progress/kendo-angular-popup';
import { ScrollViewModule } from '@progress/kendo-angular-scrollview';
import { SortableModule } from '@progress/kendo-angular-sortable';
import { UploadModule } from '@progress/kendo-angular-upload';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { DialogModule } from '@progress/kendo-angular-dialog';

import { AppComponent } from './app.component';
import { HomeComponent,PilotComponent } from './components/home/home.component';
import { AppRoutingModule } from './app-routing.module';

import { ElectronService } from './providers/electron.service';
import { BookingService } from './components/+booking/wh-booking.service';
import { BookingDetailService } from './components/+booking/wh-bookingdetail.service';
import { FlightAssignService } from './components/+booking/wh-flightassign.service';
import { AgentService } from './components/+booking/wh-agent.service';
import { NationalityService } from './components/+booking/wh-nationality.service';
import { EditService } from './components/+booking/edit.service';
import { ProductService } from './components/+booking/wh-product.service';
import { ConsultantService } from './components/+booking/wh-consultant.service';
import { CurrencyService } from './components/+booking/wh-currency.service';
import { LocationService } from './components/+booking/wh-location.service';
import { PilotService } from './components/+booking/wh-pilot.service';
import { AircraftService } from './components/+booking/wh-aircraft.service';

import { LoginService } from './components/+login/login.service';

//  components
import { AboutComponent } from './components/+about/about.component';
import { AboutUsComponent } from './components/+about/about-us.component';
import { AboutBananaComponent } from './components/+about/about-banana.component';
import { AboutApplePearComponent } from './components/+about/about-apple-pear.component';
import { KendoButtonsComponent } from './components/+buttons/kendo-buttons.component';
import { KendoChartsComponent } from './components/+charts/kendo-charts.component';
import { KendoDateInputsComponent } from './components/+dateinputs/kendo-date-inputs.component';
import { KendoDropDownsComponent } from './components/+dropdowns/kendo-dropdowns.component';
import { KendoGridComponent } from './components/+grid/kendo-grid.component';
import { KendoLabelComponent } from './components/+label/kendo-label.component';
import { KendoLayoutComponent } from './components/+layout/kendo-layout.component';
import { KendoPopupComponent } from './components/+popup/kendo-popup.component';
import { KendoScrollViewComponent } from './components/+scrollview/kendo-scrollview.component';
import { KendoSortableComponent } from './components/+sortable/kendo-sortable.component';
import { KendoUploadComponent } from './components/+upload/kendo-upload.component';
import { LoginComponent } from './components/+login/wh-login.component';
import { ContextMenuModule } from 'ngx-contextmenu';


// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// AoT requires an exported function for factories

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PilotComponent,
    LoginComponent,
    AboutComponent,
    AboutUsComponent,
    AboutBananaComponent,
    AboutApplePearComponent,
    KendoButtonsComponent,
    KendoChartsComponent,
    KendoDateInputsComponent,
    KendoDropDownsComponent,
    KendoGridComponent,
    KendoLabelComponent,
    KendoLayoutComponent,
    KendoPopupComponent,
    KendoScrollViewComponent,
    KendoSortableComponent,
    KendoUploadComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,   
    HttpClientModule,
    ButtonsModule,
    ChartsModule,
    DateInputsModule,
    DropDownsModule,
    GridModule,
    JsonpModule,
    LabelModule,
    LayoutModule,
    PopupModule,
    ScrollViewModule,
    SortableModule,
    UploadModule,
    InputsModule,
    DialogModule,
    BrowserAnimationsModule,
    NgbModalModule.forRoot(),
    CalendarModule.forRoot(),
    ContextMenuModule.forRoot({
      useBootstrap4: true
    }),    
    
    DemoUtilsModule
  ],
  entryComponents: [
    LoginComponent,
    PilotComponent
  ],
  providers: [
    LoginService,
    ProductService,
    ElectronService,
    BookingService,
    AgentService  ,
    NationalityService,
    EditService,
    BookingDetailService,
    FlightAssignService,
    ConsultantService,
    CurrencyService,
    LocationService,
    PilotService,
    AircraftService,
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
