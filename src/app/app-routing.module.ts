import { NgModule } from '@angular/core';
import { HomeComponent } from './components/home/home.component';
import { Routes, RouterModule } from '@angular/router';

// components
import { AboutComponent } from './components/+about/about.component';
import { AboutUsComponent } from './components/+about/about-us.component';
import { AboutBananaComponent } from './components/+about/about-banana.component';
import { AboutApplePearComponent } from './components/+about/about-apple-pear.component';
import { KendoButtonsComponent } from './components/+buttons/kendo-buttons.component';
import { KendoChartsComponent } from './components/+charts/kendo-charts.component';
import { KendoDateInputsComponent } from './components/+dateinputs/kendo-date-inputs.component';
import { KendoDropDownsComponent } from './components/+dropdowns/kendo-dropdowns.component';
import { KendoGridComponent } from './components/+grid/kendo-grid.component';
// import { KendoInputsComponent } from './components/+inputs/kendo-inputs.component';
import { KendoLabelComponent } from './components/+label/kendo-label.component';
import { KendoLayoutComponent } from './components/+layout/kendo-layout.component';
import { KendoPopupComponent } from './components/+popup/kendo-popup.component';
import { KendoScrollViewComponent } from './components/+scrollview/kendo-scrollview.component';
import { KendoSortableComponent } from './components/+sortable/kendo-sortable.component';
import { KendoUploadComponent } from './components/+upload/kendo-upload.component';






const routes: Routes = [
    {   path: '' , component: HomeComponent},
    {    path: 'about' , component: AboutComponent},
    {    path: 'grid' , component: KendoGridComponent},
    {    path: 'buttons'  , component: KendoButtonsComponent},
    {    path: 'dropdowns' , component: KendoDropDownsComponent},
    {    path: 'layout' , component: KendoLayoutComponent},
    {    path: 'upload' , component: KendoUploadComponent},
    {    path: 'charts' , component: KendoChartsComponent},
    {    path: 'popup' , component: KendoPopupComponent},
    {    path: 'dateinputs' , component: KendoDateInputsComponent},
    // {    path: 'inputs' , component: KendoInputsComponent},
    {    path: 'scrollview' , component: KendoScrollViewComponent},
    {    path: 'label' , component: KendoLabelComponent},
    {    path: 'sortable' , component: KendoSortableComponent}
  

];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule { }

